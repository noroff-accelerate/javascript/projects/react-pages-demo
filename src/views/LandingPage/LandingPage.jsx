/**
 * Dependencies
 * @ignore
 */
import './LandingPage.css'

/**
 * Component
 * @ignore
 */
function LandingPage() {
  return (
    <>
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
      <a
        className="LandingPage-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn React
      </a>
    </>
  )
}

/**
 * Exports
 * @ignore
 */
export default LandingPage
