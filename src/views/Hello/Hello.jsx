/**
 * Dependencies
 * @ignore
 */
import { useParams, useNavigate } from 'react-router-dom'

/**
 * Component
 * @ignore
 */
function Hello() {
  const { name } = useParams()
  const navigate = useNavigate()

  return <h1 onClick={() => navigate('/')}>Hello, {name}!</h1>
}

/**
 * Exports
 * @ignore
 */
export default Hello
