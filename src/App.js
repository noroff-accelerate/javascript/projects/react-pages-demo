import logo from './logo.svg'
import './App.css'
import LandingPage from './views/LandingPage/LandingPage.jsx'
import Hello from './views/Hello/Hello.jsx'
import { BrowserRouter, Routes, Route, NavLink } from 'react-router-dom'

function App() {
  const basename =
    process.env.NODE_ENV === 'production'
      ? '/javascript/projects/react-pages-demo/'
      : '/'

  return (
    <BrowserRouter basename={basename}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <nav>
            <li>
              <NavLink className="App-link" to="/">
                Root
              </NavLink>
            </li>
            <li>
              <NavLink className="App-link" to="/hello/World">
                Hello
              </NavLink>
            </li>
          </nav>
          <Routes>
            <Route path="/" element={<LandingPage />}></Route>
            <Route path="/hello/:name" element={<Hello />}></Route>
          </Routes>
        </header>
      </div>
    </BrowserRouter>
  )
}

export default App
